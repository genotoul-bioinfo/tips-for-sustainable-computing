---
title: "What you should do when you use the cluster"
subtitle: "Tips for a more sustainable use"
author: 
  - "Plate-forme bioinfo"
date: '`r Sys.Date()`'
output:
  xaringan::moon_reader:
    css: [xaringan-themer.css]
    lib_dir: libs
    nature:
      ratio: '16:9'
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(xaringanthemer)
style_mono_light(
  base_color = "#363636",
  code_inline_color = "#107006",
  text_bold_color = "#822744",
  header_font_google = google_font("Yanone Kaffeesatz"),
  link_color = "#ff0f87"
)
```

### Context

 - We calculated that 1 hour of computation on 1 CPU is equivalent to the emission of 5.5g of CO<sub>2</sub>.
 
 - In the Paris agreement, we stated that each person should not emit more that 2T.
 
 - If you do more than 370 kh of CPU, all your quota is exhausted.

```{r, echo=FALSE, out.width="50%", fig.cap="Resources used for the cluster"}
knitr::include_graphics("images/resources.png")
```
---

### Your data

First question: *Do you need to do this computation ?*

 - Research is working for a better world.
 
 - The computation *will* need some power, cooling, etc.
 
 - Is it worth it?

---

### Your software

 - Is your software *efficient*?
 
 - There are usually many possible software for a task.
   The trade-off speed/quality sometimes varies.
 
 - Consider a fast tool first.
 
| Slow tool    | Fast tool  |
|:------------:|:----------:|
| BlastP       | Diamond    |
| SOAPdenovo2  | SSPACE     |
| MEGAHIT      | Abyss      |
| MEGAHIT      | MetaVelvet |
| metaSPAdes   | MetaVelvet |
| Centrifuge   | Kraken2    |
| STAR, HISAT2 | Salmon     |

See [a related publication](https://academic.oup.com/mbe/article/39/3/msac034/6526403)

---

### Your RAM

 - You can book your RAM for a job.
 
 - Booking too much RAM is easy.
 
 - But it also prevents the other users from using the same machine.
 
 - So, we have to buy more machine, or more RAM.
 
 - Try to be as precise as possible.
 
 - Yes, it requires trial-and-error on one dataset.

---

### Your project

 - Your personnal eCO<sub>2</sub> quota is provided when you log in.

 - For legal reasons, we cannot connect the account of the trainee/student/post-doc to a project or a PI.
 
 - However, the trainee/student/post-doc will most likely perform the computational tasks.

 - It is the responsability of the PI to monitor the eCO<sub>2</sub> quota of the project and the people working on it.
